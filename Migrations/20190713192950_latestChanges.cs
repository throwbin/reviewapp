﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ReviewApp.Migrations
{
    public partial class latestChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Image",
                table: "Article");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "Article");

            migrationBuilder.RenameColumn(
                name: "Genre",
                table: "Series",
                newName: "Genres");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Genres",
                table: "Series",
                newName: "Genre");

            migrationBuilder.AddColumn<string>(
                name: "Image",
                table: "Article",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Article",
                nullable: true);
        }
    }
}
