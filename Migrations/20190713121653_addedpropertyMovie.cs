﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ReviewApp.Migrations
{
    public partial class addedpropertyMovie : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ArticleId",
                table: "Series",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ArticleId",
                table: "Movies",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Overview",
                table: "Movies",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Article",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(nullable: true),
                    ArticleText = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    TrailerUrl = table.Column<string>(nullable: true),
                    ImdbLink = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Article", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Series_ArticleId",
                table: "Series",
                column: "ArticleId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_ArticleId",
                table: "Movies",
                column: "ArticleId");

            migrationBuilder.AddForeignKey(
                name: "FK_Movies_Article_ArticleId",
                table: "Movies",
                column: "ArticleId",
                principalTable: "Article",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Series_Article_ArticleId",
                table: "Series",
                column: "ArticleId",
                principalTable: "Article",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Movies_Article_ArticleId",
                table: "Movies");

            migrationBuilder.DropForeignKey(
                name: "FK_Series_Article_ArticleId",
                table: "Series");

            migrationBuilder.DropTable(
                name: "Article");

            migrationBuilder.DropIndex(
                name: "IX_Series_ArticleId",
                table: "Series");

            migrationBuilder.DropIndex(
                name: "IX_Movies_ArticleId",
                table: "Movies");

            migrationBuilder.DropColumn(
                name: "ArticleId",
                table: "Series");

            migrationBuilder.DropColumn(
                name: "ArticleId",
                table: "Movies");

            migrationBuilder.DropColumn(
                name: "Overview",
                table: "Movies");
        }
    }
}
