﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ReviewApp.DatabaseContexts;
using ReviewApp.Models;
using ReviewApp.Services;

namespace ReviewApp.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        private DatabaseContext _context;

        public AccountController(DatabaseContext context)
        {
            _context = context;

        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UserLogin userLogin)
        {
            if (!string.IsNullOrEmpty(userLogin.UserName) && string.IsNullOrEmpty(userLogin.Password))
            {
                return RedirectToAction("Login");
            }

            if (ModelState.IsValid)
            {
                var userProfileDb = _context.UserRegistrations.
                    FirstOrDefault(a => a.UserName.Equals(userLogin.UserName));

                if (userProfileDb == null)
                {
                    return RedirectToAction("Login");
                }

                var userProfileExists = CryptoService.VerifyPassword(userLogin.Password, 
                    Convert.FromBase64String(userProfileDb.PasswordSalt),
                    Convert.FromBase64String(userProfileDb.PasswordHash));

                if (userProfileExists)
                {
                    ClaimsIdentity identity;

                    if (userProfileDb.UserName == "gianni")
                    {
                        //Create the identity for the user
                        identity = new ClaimsIdentity(new[] {
                            new Claim(ClaimTypes.Name, userProfileDb.UserName),
                            new Claim(ClaimTypes.Role, "Admin")
                        }, CookieAuthenticationDefaults.AuthenticationScheme);

                    }
                    else
                    {
                        //Create the identity for the user
                        identity = new ClaimsIdentity(new[] {
                            new Claim(ClaimTypes.Name, userProfileDb.UserName),
                            new Claim(ClaimTypes.Role, "User")
                        }, CookieAuthenticationDefaults.AuthenticationScheme);
                    }


                    var principal = new ClaimsPrincipal(identity);

                    var login = HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

                    return RedirectToAction("Index", "Home");
                }

            }

            return View();
        }

        [HttpGet]
        public IActionResult CreateLogin()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateLogin(UserRegistration userRegistration)
        {
            if (ModelState.IsValid)
            {
                if (userRegistration.PasswordConfirm == userRegistration.Password)
                {
                    if (!_context.UserRegistrations.Any(x => x.UserName == userRegistration.UserName))
                    {
                        await _context.UserRegistrations.AddAsync(userRegistration);
                    }
                }

                await _context.SaveChangesAsync();

                return RedirectToAction("Login");

            }
            return View();
        }

        [HttpGet]
        public IActionResult Logout()
        {
            var login = HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            HttpContext.Response.Cookies.Delete("UserName");
            return RedirectToAction("Login");
        }

        public JsonResult UserNameNotExist(string userName)
        {
            var userRegistration = _context.UserRegistrations.FirstOrDefault(a => a.UserName.Equals(userName));

            if (userRegistration == null)
                return Json("Gebruiker bestaat niet, maak een nieuw account aan.");

            return Json(true);
        }

        public JsonResult UserNameExist(string userName)
        {
            var userRegistration = _context.UserRegistrations.FirstOrDefault(a => a.UserName.Equals(userName));

            if(userRegistration != null) 
                return Json($"{userRegistration.UserName} is not available");

            return Json(true);
        }
    }
}