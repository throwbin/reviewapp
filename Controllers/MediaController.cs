﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.DataAnnotations.Internal;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ReviewApp.DatabaseContexts;
using ReviewApp.DbHandlers;
using ReviewApp.Models;
using ReviewApp.TMDb;

namespace ReviewApp.Controllers
{
    [Authorize(Roles = "Admin, User")]
    public class MediaController : Controller
    {
        private readonly DatabaseContext _context;
        private MediaDbHandler _mDbHandler;

        public MediaController(DatabaseContext context, MediaDbHandler mvDbHandler)
        {
            _mDbHandler = mvDbHandler;
            _context = context;
        }

        public async Task<IActionResult> Movies()
        {
            var movies = await _mDbHandler.GetAllMovies();
            return View(movies);
        }

        public async Task<IActionResult> Series()
        {
            var series = await _mDbHandler.GetAllSeries();
            return View(series);
        }

        public IActionResult Article(int id, string type)
        {
            switch (type)
            {
                case "movie":
                    {
                        var article = _context.Movies.Include(x => x.Article).First(z => z.Id == id);
                        return View(article);
                    }

                case "series":
                    {
                        var article = _context.Series.Include(x => x.Article).First(z => z.Id == id);
                        return View(article);

                    }
            }

            return default;
        }



        public async Task<RedirectToActionResult> GetFromTmDb(string type)
        {
            switch (type)
            {
                case "movie":
                    {
                        var ret = await TmDbHandler.GetPopular<List<Movie>>();
                        ret.ForEach(x =>
                            {
                                x.Article.TrailerUrl = "https://www.youtube.com/embed/" + x.Article.TrailerUrl;
                                x.Article.ImdbLink = "https://www.imdb.com/title/" + x.Article.ImdbLink;
                                x.Image = "https://image.tmdb.org/t/p/w500/" + x.Image;

                            });
                        await _context.Movies.AddRangeAsync(ret);
                        break;
                    }

                case "series":
                    {
                        var ret = await TmDbHandler.GetPopular<List<Series>>();
                        ret.ForEach(x =>
                        {
                            x.Article.TrailerUrl = "https://www.youtube.com/embed/" + x.Article.TrailerUrl;
                            x.Article.ImdbLink = "https://www.imdb.com/title/" + x.Article.ImdbLink;
                            x.Image = "https://image.tmdb.org/t/p/w500/" + x.Image;

                        });
                        await _context.Series.AddRangeAsync(ret);
                        break;
                    }
            }

            await _context.SaveChangesAsync();

            return RedirectToAction("Index", "Home");

        }

        public IActionResult CreateNewEntry()
        {
            return View();
        }

        [HttpPost]
        public async Task<RedirectToActionResult> CreateNewEntryForMovie(Movie movie)
        {
            await _context.Movies.AddAsync(movie);
            await _context.SaveChangesAsync();
            return RedirectToAction("Movies", "Media");

        }

        [HttpPost]
        public async Task<RedirectToActionResult> CreateNewEntryForSeries(Series series)
        {
            await _context.Series.AddAsync(series);
            await _context.SaveChangesAsync();
            return RedirectToAction("Series", "Media");
        }

        [Authorize(Roles = "Admin")]
        public async Task<RedirectToActionResult> TruncateSeries()
        {
            var res = _context.Series.Select(x => x.Article.Id).ToList();
            int ret = 0;

            ret = await _context.Database.ExecuteSqlCommandAsync("TRUNCATE TABLE [Series]");

            foreach (var id in res)
            {
              ret = await _context.Database.ExecuteSqlCommandAsync($"DELETE FROM [dbo].[Article] WHERE Id = {id}");
            }

            return RedirectToAction("Index", "Home");

        }


        [Authorize(Roles = "Admin")]
        public async Task<RedirectToActionResult> TruncateMovies()
        {
            var res = _context.Movies.Select(x => x.Article.Id).ToList();
            int ret = 0;

            ret = await _context.Database.ExecuteSqlCommandAsync("TRUNCATE TABLE [Movies]");

            foreach (var id in res)
            {
                ret = await _context.Database.ExecuteSqlCommandAsync($"DELETE FROM [dbo].[Article] WHERE Id = {id}");
            }

            return RedirectToAction("Index", "Home");
        }

    }
}
