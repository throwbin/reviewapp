﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ReviewApp.Helpers;
using ReviewApp.Models;

namespace ReviewApp.TMDb
{
    // ReSharper disable once InconsistentNaming


    public static class TmDbHandler
    {


        public static async Task<T> GetPopular<T>(bool ascending = false) where T : new()
        {
            string t = typeof(T).GetGenericArguments()[0].Name;
            string mediaName;

            switch (t)
            {
                case "Series":
                    mediaName = "tv";
                    break;
                case "Movie":
                    mediaName = "movie";
                    break;
                default:
                    throw new NotImplementedException();
            }

            string url =
                $"http://api.themoviedb.org/3/discover/{mediaName}?&sort_by=popularity.{(ascending ? "asc" : "desc")}&page=1&api_key=af1a9e582bd4e38311a217b44c941e8a";
            var (jsonResult, statusCode) = await Helper.MakeRequest("GET",
                 url);

            if (statusCode && jsonResult.Any())
            {
                JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings
                {
                    MissingMemberHandling = MissingMemberHandling.Ignore,
                    NullValueHandling = NullValueHandling.Ignore,
                    TypeNameHandling = TypeNameHandling.All
                };

                var type = typeof(T);
                var popularMedia = JsonConvert.DeserializeObject(jsonResult["results"].ToString(), 
                    type, jsonSerializerSettings);


                return (T)popularMedia;
            }

            return default;
        }
    }
}
