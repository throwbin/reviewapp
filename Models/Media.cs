﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ReviewApp.Helpers;

namespace ReviewApp.Models
{
    public abstract class Media
    {
        [NotMapped]
        private int _apiId;

        [NotMapped]
        private List<int> _apiGenres;

        [NotMapped]
        private string _image;

        [NotMapped]
        [JsonProperty("id")]
        private int ApiId
        {
            get => _apiId;
            set
            {
                _apiId = value;
                ImdbUrl = value.ToString();
                VideoUrl = value.ToString();

            }
        }

        public int Id { get; set; }

        [Required]
        public abstract string Title { get; set; }

        [NotMapped]
        [JsonProperty("genre_ids")]
        private List<int> GenresFromApi
        {
            get => _apiGenres;
            set
            {
                _apiGenres = value;

                var allGenres = GetGenresFromApi().Result;
                Genres = string.Join(", ", SearchGenres(value.ToArray(), allGenres));
            }
        }

        [Required]
        public string Genres { get; set; }

        [JsonProperty("poster_path")]
        [Required]
        public string Image {
            get => _image;
            set => _image = value;
        }

        [JsonProperty("vote_average")]
        [Required]
        public string Rating { get; set; }


        [JsonProperty("Article")]
        [Required]
        public Article Article { get; set; }

        [NotMapped]
        [JsonProperty("overview")]
        private string Overview
        {
            get => Article?.ArticleText;
            set
            {
                if (Article == null) { Article = new Article(); }
                Article.ArticleText = value;
            }
        }

        [NotMapped]
        [Required]
        private string ImdbUrl
        {
            get => Article?.ImdbLink;
            set
            {
                if (Article == null) { Article = new Article(); }

                if (int.TryParse(value, out int result))
                {
                    Article.ImdbLink = GetImdbLinkFromId(result).Result;
                }
            }
        }

        [NotMapped]
        [Required]
        private string VideoUrl
        {
            get => Article?.TrailerUrl;
            set
            {
                if (Article == null) { Article = new Article(); }

                if (int.TryParse(value, out int result))
                {
                    Article.TrailerUrl = GetVideoUrlFromId(result).Result;
                }
            }
        }

        [Required]
        public abstract DateTime ReleaseDate { get; set; }
        public DateTime EntryDate { get; set; } = DateTime.Now;

        private static async Task<string> GetVideoUrlFromId(int id)
        {
            var (ret, isSuccessStatusCode) = await Helper.MakeRequest("GET",
                $"http://api.themoviedb.org/3/movie/{id}/videos?api_key=af1a9e582bd4e38311a217b44c941e8a");

            if (isSuccessStatusCode)
            {
              
                var urls = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>
                    (ret["results"].ToString());

                if (urls != null && urls.Any() && urls[0]["site"].ToString().Contains("YouTube"))
                {
                    return urls[0]["key"].ToString();
                }
                
            }


            return default;
        }

        private static async Task<string> GetImdbLinkFromId(int id)
        {
            var (res, isSuccessStatusCode) = await Helper.MakeRequest("GET",
                $"https://api.themoviedb.org/3/movie/{id}/external_ids?api_key=af1a9e582bd4e38311a217b44c941e8a&language=en-US");

            if (isSuccessStatusCode)
            {
                return res["imdb_id"].ToString();
            }

            return default;
        }

        private static async Task<List<Genre>> GetGenresFromApi()
        {
            var (res, statusCode) = await Helper.MakeRequest("GET",
                "https://api.themoviedb.org/3/genre/movie/list?api_key=af1a9e582bd4e38311a217b44c941e8a&language=en-US");

            try
            {
                return JsonConvert.DeserializeObject<List<Genre>>(res["genres"].ToString());

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return default;

        }

        private static List<string> SearchGenres(int[] ids, List<Genre> genres)
        {
            return genres.Where(x => ids.Contains(x.Id)).Select(z => z.Name).ToList();
        }
    }


}
