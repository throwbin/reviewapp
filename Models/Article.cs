﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ReviewApp.Models
{
    public class Article
    {
        public int Id { get; set; }

        [Required]
        public string ArticleText { get; set; }

        [Required]
        public string TrailerUrl { get; set; }

        [Required]
        public string ImdbLink { get; set; }
    }
}
