﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ReviewApp.Services;

namespace ReviewApp.Models
{
    public class UserRegistration
    {
        private string _password;

        public int Id { get; set; }

        [Required(ErrorMessage = "Gebruikersnaam is niet ingevuld.")]
        [StringLength(30)]
        [RegularExpression(@"^[_A-z0-9]*((-|\s)*[_A-z0-9])*$", 
            ErrorMessage = "UserName mag geen speciale tekens bevatten")]
        [Remote("UserNameExist", "Account")]
        public string UserName { get; set; }


        [NotMapped]
        [Required(ErrorMessage = "Wachtwoord is niet ingevuld.")]
        [StringLength(10)]
        public string Password {
            get => _password;
            set
            {
                _password = value;

                var passwordSalt = CryptoService.GenerateSalt();

                PasswordSalt = Convert.ToBase64String(passwordSalt);
                PasswordHash = Convert.ToBase64String(
                    CryptoService.ComputeHash(value, passwordSalt));
            }
        }

        [NotMapped]
        [Required(ErrorMessage = "Hertyp het wachtwoord.")]
        [StringLength(10)]
        [Compare("Password", 
            ErrorMessage = "De wachtwoorden komen niet overeen, probeer het opnieuw.")]
        public string PasswordConfirm { get; set; }

        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }

    }
}
