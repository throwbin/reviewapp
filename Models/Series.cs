﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ReviewApp.Models
{
    public class Series : Media
    {
        [JsonProperty("original_name")]
        public sealed override string Title { get; set; }

        [JsonProperty("first_air_date")]
        public sealed override DateTime ReleaseDate { get; set; }
    }
}
