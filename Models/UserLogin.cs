﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ReviewApp.Models
{
    [NotMapped]
    public class UserLogin
    {
        [Required(ErrorMessage = "Gebruikersnaam is niet ingevuld.")]
        [StringLength(30)]
        [RegularExpression(@"^[_A-z0-9]*((-|\s)*[_A-z0-9])*$",
            ErrorMessage = "UserName mag geen speciale tekens bevatten")]

        [Remote("UserNameNotExist", "Account")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Wachtwoord is niet ingevuld.")]
        [StringLength(10)]
        public string Password { get; set; }
    }
}
