﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ReviewApp.DatabaseContexts;
using ReviewApp.Models;

namespace ReviewApp.DbHandlers
{
    public class MediaDbHandler
    {
        private DatabaseContext _context;

        public MediaDbHandler(DatabaseContext context)
        {
            _context = context;
        }

        public async Task<List<Movie>> GetAllMovies()
        {
            return await _context.Movies.ToListAsync();
        }

        public async Task<List<Series>> GetAllSeries()
        {
            return await _context.Series.ToListAsync();
        }
    }
}
