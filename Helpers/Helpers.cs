﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ReviewApp.Helpers
{
    public static class Helper
    {
        public static async Task<(Dictionary<string, object>, bool IsSuccessStatusCode)> MakeRequest(string method, string url, string content = "")
        {
            string jsonResponse = "";
            HttpResponseMessage response = null;

            JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings
            {
                MissingMemberHandling = MissingMemberHandling.Ignore
            };


            using (var client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMinutes(10);

            

                switch (method.ToUpper())
                {
                    case "POST":
                        response = await client.PostAsync(
                            url,
                            new StringContent(content, Encoding.UTF8, "application/json"));
                        jsonResponse = await response.Content.ReadAsStringAsync();
                        break;
                    case "GET":
                        response = await client.GetAsync(
                            url);
                        jsonResponse = await response.Content.ReadAsStringAsync();
                        break;
                    case "PUT":
                        response = await client.PutAsync(
                            url,
                            new StringContent(content, Encoding.UTF8, "application/json"));
                        jsonResponse = await response.Content.ReadAsStringAsync();
                        break;
                    case "DELETE":
                        response = await client.DeleteAsync(
                            url);
                        jsonResponse = await response.Content.ReadAsStringAsync();
                        break;
                }
            }

            
            return (JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonResponse, jsonSerializerSettings), response != null && response.IsSuccessStatusCode);
        }

    }
}
